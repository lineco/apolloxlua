/***
 * 
 */
;exports[{*blocks.CatchClause*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
        var e = eax;
        var block = "";
        if(e.body) {
             block = GetValueByType(e.body);
        }
        var param = "";
        if(e.param) {
              param = GetValueByType(e.param);
        }
       
        var str = " local  "+param+" = err \n" + 
            block + "\n"; 
        return str;
{% end %}
}

