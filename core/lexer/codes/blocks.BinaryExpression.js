/***
 * 
 */
;exports[{*blocks.BinaryExpression*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
  var left = GetValueByType(eax.left);
  var right= GetValueByType(eax.right);
  
  switch (eax.operator) {
        case "!=":
        eax.operator = "~=";
        break;
        case "+":
        var b1 = isString(left);
        var b2 = isString(right);
        if( b1 || b2 ) {
            eax.operator = "..";
        }
        break;
        case ">>>":
        var b1 = isNumber(right);
        if(b1) {
            break;
        }
        
        eax.operator = ":";
        break;
  }
  
  if(right == "null") {
        right = "nil"
  }
  
  return  left + eax.operator + right;
{% end %}
}

