;exports[{*blocks.ForStatement*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
         var e = eax;
         var codes = [];
         
         var init   =  GetValueByType( e.init);   
         var update = GetValueByType(e.update);  
         var test   =  GetValueByType(e.test);  
         codes.push(init);   
         codes.push("\nwhile " +test+ " do\n");
         codes.push(GetValueByType (
            e.body
         ));
         codes.push("\n"+update+";\n");
         codes.push("end\n");
         return codes.join("");
{% end %}
}

