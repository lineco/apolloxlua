/***
 * 
 */
;exports[{*blocks.FunctionDeclaration*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
        var e = eax;
        var params = [];
            
        var functionName = GetValueByType(e.id);
                    
        if(e.params) {
            var len = e.params.length;
            if(len > 0) {
                for(var i = 0; i < len; i++)  {
                    params[i] = GetValueByType(
                            e.params[i]
                        )
                }
            }
        }
        
        var codes ="local " +  functionName + "=function "+"("+params.join(",")+")  \n";
        if(e.body) {
               codes += GetValueByType(
                            e.body
                        );
        }
        codes += "\nend\n";
        return codes;

{% end %}
}

