;exports[{*blocks.ForInStatement*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
         var e = eax;
            
         var left  =  GetValueByType(e.left); 
                 
         var right =  GetValueByType(e.right); 
                 
         var body  = GetValueByType (e.body);
                             
         var code = [];
        
         code.push( "for " + left + ", v in pairs(" +right+") do" );
         code.push(body);
         code.push("end");
         return code.join("\n");
{% end %}
}

