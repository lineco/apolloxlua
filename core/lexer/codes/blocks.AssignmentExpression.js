/***
 * 
 */
;exports[{*blocks.AssignmentExpression*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
  
  var left = GetValueByType(eax.left);
  var right= GetValueByType(eax.right);
  
  switch (eax.operator) {
      case "+=":
      return left +"=" +left + "+" +right;
      case "-=":
      return left +"=" +left + "-" +right;
      case "*=":
      return left +"=" +left + "*" +right;
      case "/=":
      return left +"=" +left + "/" +right;
      case "%=":
      return left +"=" +left + "%" +right;
  }
  return  left + eax.operator + right;
{% end %}
}
