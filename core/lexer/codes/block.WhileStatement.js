/***
 * 
 */
;exports[{*blocks.WhileStatement*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
         var e = eax;
         var codes = [];
            
        if(e.test) {
            var test  =  GetValueByType(e.test);  
            codes.push("while " +test+ " do \n");
        }
        
        if(e.body) {
                codes.push(GetValueByType (
                                e.body
                        ));
        }
        codes.push("\nend\n");
        return codes.join("");
{% end %}
}

