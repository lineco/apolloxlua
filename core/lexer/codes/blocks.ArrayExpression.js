/***
 * 
 */
;exports[{*blocks.ArrayExpression*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
           var e = eax;
           if(e && e.elements) {
                var len = e.elements.length ;
                
                depencies["array"] = true;
                if(len == 0) { /// []
                    return "{}";
                } else {
                    var codes = [];
                    for( var i=0; i< len; i++) {
                        codes.push(GetValueByType(
                                e.elements[i]
                            ));
                    }
                    
                    var len = codes.length;
                    
                    var codeN = [];
                    for( var i = 1; i<= len ; i++) {
                        if(i == 1) {
                             codeN[i] = "["+i+"]="+codes[i-1];
                        } else {
                             codeN[i] = "\n["+i+"]="+codes[i-1];
                        }
                    }
                    codeN.shift();
                    return   "{"+codeN.join(",") +"}"
                }
            }
            
            return "null";
{% end %}
}