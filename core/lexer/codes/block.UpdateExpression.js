/***
 * 
 */
;exports[{*blocks.UpdateExpression*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
    var target = GetValueByType(eax.argument);
    if(eax.operator == "++") {
        return target + "=" + target + "+1";
    } 
    return target + "=" + target + "-1";
{% end %}
}
