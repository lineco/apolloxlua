/***
 * 
 */
;exports[{*blocks.FunctionExpression*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
        var e = eax;
        var params = [];
        if(e.params) {
            var len = e.params.length;
            if(len > 0) {
                for(var i = 0; i < len; i++)  {
                    params[i] = GetValueByType(e.params[i])
                }
            }
        }
        var code  = "function ("+params.join(",")+") \n";
        if(e.body) {
            code +=  GetValueByType (e.body);
        }
        code += "\nend\n";
        return code;
{% end %}
}

