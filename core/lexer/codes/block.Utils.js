{% if  SCRIPT  == "lua" then %}

var isNumber = function (str) {
   return false == isNaN(Number(str));
}
var isString = function (str) {
    var pos1= str.indexOf("\"");
    var pos2= str.lastIndexOf("\"");
    return pos1 != -1 && pos2 != -1 && pos1 != pos2;
}

var getRandomName = function () {
    var str = "CODE"+ ((new Date().getTime()+"").substr(6,6)) + 
    ((Math.random()) * 1000 >> 0 )+ 
    (Math.random() * 1000 >> 0)+
    (Math.random() * 1000 >> 0)+
    (Math.random() * 1000 >> 0)+ (Math.random() * 30 > 15?"H":"L");
    return str;
}

var makeLuaTable = function  (name, data) {
     var codes = [];
     for(var k in data) {
         codes.push("[" + k + "]=\"" + data[k] + "\"");
     }
     return "{\n" + codes.join(",\n") + "\n}";
}



{% end %}

var decodeSecurity = function (str) {
    var arr = str;
    
    var len = arr.length;
    for(var i = 0; i< len; i++) {
       arr[i] =  255 - (arr[i])>> 0;;
    }
    var uint8array = new Uint8Array(arr);
    var len = uint8array.byteLength;
    var string = "";
    var i = 0;
    while (i < len) {
        var c = uint8array[i];
        if (c < 128) {
            string += String.fromCharCode(c);
            i++;
        } else if((c > 191) && (c < 224) && (i + 1 < len)) {
            string += String.fromCharCode(((c & 31) << 6) | (uint8array[i + 1] & 63));
            i += 2;
        } else if(i + 2 < len) {
            string += String.fromCharCode(((c & 15) << 12) | ((uint8array[i + 1] & 63) << 6) | (uint8array[i + 2] & 63));
            i += 3;
        }
    }
    return string;
}