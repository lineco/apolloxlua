/***
 * 
 */
;exports[{*blocks.VariableDeclarator*}] = function (eax) {
{% if  SCRIPT  == "lua" then %}
        var id = eax.id;
        var va = eax.init;
        var valName = GetValueByType(id);
        var valData = GetValueByType(va);
        if(valData) {
            return "local " + valName + "=" + valData;
        }
        return valName;
{% end %}
}