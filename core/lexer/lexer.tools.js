{% if  PLAT  == "tool" then %}
var ps    = require('child_process');
var fiber = (function (){
        /**
         * 纤程执行
         */
        FiberExecute = function (funDone) {
            this.tail = null;
            this.queue= [];
            this.errors= [];
            this.handle=funDone;
            this.zero = function (){
                this.queue = [];
                this.errors= [];
                this.tail = null;
            }
            this.onError = function (e) {
                this.errors.push(e);
            }
            this.start = function(){
                this.next();
            }
            this.next = function (){
                this.tail = this.queue.shift();
                if(this.tail && this.tail.hasOwnProperty('execute')){
                    this.tail.execute();
                } else {
                    if(this.handle != null) {
                        this.handle (this.errors.length > 0 ?this.errors:null);
                    }
                }
            }
            this.add = function (v) {
                v.add(this);
                this.queue.push(v);
            }
            this.length = function () {
                return this.queue.length;
            }
        }
        FiberTask = function (cmd ,fParams){
            var params = fParams;
            var parent = null;
            var command= cmd;
            
            this.getFiberName= function () {
                return fiberName;
            }
            this.add = function (e){
                parent = e;
            }
            this.execute = function ( ) {
               var child = ps.exec(command ,
                    function (err, data) {
            	        
                        if( err ){
                            parent.onError(err);
                            parent.next();
                            console.log(err);
                            return;
                        }
                        if( data ){
                            if(FiberTask.prototype.onTaskDone != null){
                                var buff = new Buffer(data);
                                var str  = buff? buff.toString("utf-8"):"";
                                if(params) {
                                	FiberTask.prototype.onTaskDone(params, buff);
                                }
                            } else {
                                console.log("not find callback");
                            }
                        }
                        if(parent.hasOwnProperty('next')) {
                            parent.next();
                        }
                 })
            }
        }
        return {
            "FiberExecute":FiberExecute,
            "FiberTask":FiberTask
        }
})();
exports.FiberExecute   = fiber.FiberExecute ;
exports.FiberTask      = fiber.FiberTask;
{% end %}