{-inline_listense-}
/***
The MIT License (MIT)

Copyright (c) 2018 agent.zy@aliyun.com

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
{-inline_listense-}


{*blocks.inline_listense*}

{-VariableDeclaration-}
"VariableDeclaration"
{-VariableDeclaration-}


{-VariableDeclarator-}
"VariableDeclarator"
{-VariableDeclarator-}


{-Identifier-}
"Identifier"
{-Identifier-}

{-Literal-}
"Literal"
{-Literal-}

{-BinaryExpression-}
"BinaryExpression"
{-BinaryExpression-}

{-CallExpression-}
"CallExpression"
{-CallExpression-}

{-MemberExpression-}
"MemberExpression"
{-MemberExpression-}

{-ExpressionStatement-}
"ExpressionStatement"
{-ExpressionStatement-}


{-AssignmentExpression-}
"AssignmentExpression"
{-AssignmentExpression-}

{-UpdateExpression-}
"UpdateExpression"
{-UpdateExpression-}

{-ObjectExpression-}
"ObjectExpression"
{-ObjectExpression-}

{-IfStatement-}
"IfStatement"
{-IfStatement-}

{-BlockStatement-}
"BlockStatement"
{-BlockStatement-}

{-ArrayExpression-}
"ArrayExpression"
{-ArrayExpression-}

{-BreakStatement-}
"BreakStatement"
{-BreakStatement-}

{-CatchClause-}
"CatchClause"
{-CatchClause-}

{-TryStatement-}
"TryStatement"
{-TryStatement-}

{-ThisExpression-}
"ThisExpression"
{-ThisExpression-}

{-Property-}
"Property"
{-Property-}

{-FunctionExpression-}
"FunctionExpression"
{-FunctionExpression-}

{-FunctionDeclaration-}
"FunctionDeclaration"
{-FunctionDeclaration-}

{-ReturnStatement-}
"ReturnStatement"
{-ReturnStatement-}

{-UnaryExpression-}
"UnaryExpression"
{-UnaryExpression-}

{-WhileStatement-}
"WhileStatement"
{-WhileStatement-}

{-ConditionalExpression-}
"ConditionalExpression"
{-ConditionalExpression-}

{-LogicalExpression-}
"LogicalExpression"
{-LogicalExpression-}

{-ForStatement-}
"ForStatement"
{-ForStatement-}

{-ForInStatement-}
"ForInStatement"
{-ForInStatement-}

{-SwitchStatement-}
"SwitchStatement"
{-SwitchStatement-}

{-SwitchCase-}
"SwitchCase"
{-SwitchCase-}
